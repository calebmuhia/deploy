<?php

/*{{{ v.151005.001 (0.0.2)

	Sample config file for bitbucket hooks.

	Based on 'Automated git deployment' script by Jonathan Nicoal:
	http://jonathannicol.com/blog/2013/11/19/automated-git-deployments-from-bitbucket/

	See README.md and config.sample.php

	---
	Igor Lilliputten
	mailto: igor at lilliputten dot ru
	http://lilliputtem.ru/

	Ivan Pushkin
	mailto: iv dot pushk at gmail dot com

}}}*/

/*{{{ Auxiliary variables, used only for constructing $CONFIG and $PROJECTS  */

$REPOSITORIES_PATH = '/root/projects/mp';
$PROJECTS_PATH     = '/root/projects/mp';

/*}}}*/

// Base tool configuration:
$CONFIG = array(
	'bitbucketUsername' => 'justinissofriendly', // The username or team name where the
	// repository is located on bitbucket.org, *REQUIRED*

	'gitCommand'       => 'git',              // Git command, *REQUIRED*
	'repositoriesPath' => $REPOSITORIES_PATH, // Folder containing all repositories, *REQUIRED*
	'log'              => true,               // Enable logging, optional
	'logFile'          => '/root/projects/logs/bitbucket.log',    // Logging file name, optional
	'logClear'         => true,               // clear log each time, optional
	'verbose'          => true,               // show debug info in log, optional
	'folderMode'       => 0777,               // creating folder mode, optional
);

// List of deploying projects:
$PROJECTS = array(
	'mobile_patterns' => array( // The key is a bitbucket.org repository name
		'branch' => array(
			'deployPath'  => $PROJECTS_PATH.'/mobile_patterns', // Path to deploy project, *REQUIRED*

		),
	),

	
);
